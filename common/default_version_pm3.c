#include "common.h"
/* Generated file, do not edit */
#ifndef ON_DEVICE
#define SECTVERSINFO
#else
#define SECTVERSINFO __attribute__((section(".version_information")))
#endif

const struct version_information_t SECTVERSINFO g_version_information = {
    VERSION_INFORMATION_MAGIC,
    1,
    1,
    2,
    "Iceman/master/v4.18994",
    "2024-09-10 15:40:28",
    "4ecb7df89"
};
