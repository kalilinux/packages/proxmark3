proxmark3 (4.18994-0kali2) kali-dev; urgency=medium

  * Add upstream patches to fix flaky test

 -- Arnaud Rebillout <arnaudr@kali.org>  Fri, 13 Sep 2024 20:20:08 +0700

proxmark3 (4.18994-0kali1) kali-dev; urgency=medium

  * New upstream version 4.18994
  * Install udev rules in /usr/lib instead of /lib
  * Update tool list

 -- Arnaud Rebillout <arnaudr@kali.org>  Wed, 11 Sep 2024 22:55:22 +0700

proxmark3 (4.18589-0kali2) kali-dev; urgency=medium

  * Bump Standards-Version (no changes needed)
  * Keep silencing lintian regarding firmware
  * Adjust package short description for -doc package

 -- Arnaud Rebillout <arnaudr@kali.org>  Tue, 27 Aug 2024 10:49:05 +0700

proxmark3 (4.18589-0kali1) kali-dev; urgency=medium

  * New upstream version 4.18589
  * Update debian/copyright
  * Add new build-dep python3-bitstring
  * Refresh patches
  * Update build dependency: pkg-config => pkgconf

 -- Arnaud Rebillout <arnaudr@kali.org>  Mon, 26 Aug 2024 11:30:45 +0700

proxmark3 (4.18341-0kali1) kali-experimental; urgency=medium

  * New upstream version 4.18341
  * Add new build-dep liblz4-dev for new release
  * Install new tools
  * Add patches to fix the build

 -- Sophie Brun <sophie@offensive-security.com>  Fri, 17 May 2024 17:16:34 +0200

proxmark3 (4.16191-0kali1) kali-dev; urgency=medium

  * New upstream version 4.16191
  * Update debian/copyright
  * Update lintian-overrides
  * Bump Standards-Version to 4.6.2

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 20 Feb 2023 14:55:47 +0100

proxmark3 (4.15864-0kali1) kali-dev; urgency=medium

  * New upstream release
  * Refresh Debian files

 -- Steev Klimaszewski <steev@kali.org>  Fri, 04 Nov 2022 14:30:01 -0500

proxmark3 (4.14831-0kali4) kali-dev; urgency=medium

  * Import the upstream patch to fix the armhf build failure (and remove the
    workaround)

 -- Sophie Brun <sophie@offensive-security.com>  Fri, 04 Feb 2022 10:40:25 +0100

proxmark3 (4.14831-0kali3) kali-dev; urgency=medium

  * Improve the patch for armhf

 -- Sophie Brun <sophie@offensive-security.com>  Thu, 03 Feb 2022 16:51:42 +0100

proxmark3 (4.14831-0kali2) kali-dev; urgency=medium

  * Fix debian/copyright (copyright dates and License)
  * Add a workaround to fix a build failure on armhf

 -- Sophie Brun <sophie@offensive-security.com>  Wed, 02 Feb 2022 16:43:40 +0100

proxmark3 (4.14831-0kali1) kali-dev; urgency=medium

  [ Sophie Brun ]
  * Improve packages descriptions in debian/control
  * New upstream version 4.14831
  * Remove patches merged upstream
  * Update debian/copyright

  [ Ben Wilson ]
  * Remove template comment and switch spaces to tabs

 -- Sophie Brun <sophie@offensive-security.com>  Fri, 14 Jan 2022 15:51:54 +0100

proxmark3 (4.14434+git20211007-0kali3) kali-dev; urgency=medium

  * Remove useless dependency lua-any
  * Add python3-all-dev in build-depends to get python support in client
  * proxmark3-common: remove useless dependencies python3-*
  * Import upstream patch to change firmware directory
  * Adapt the installation and fix lintian-overrides for new firmware directory

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 12 Oct 2021 11:40:59 +0200

proxmark3 (4.14434+git20211007-0kali2) kali-dev; urgency=medium

  * Fix the package proxmark3-firmwares: it's arch all

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 11 Oct 2021 11:44:55 +0200

proxmark3 (4.14434+git20211007-0kali1) kali-dev; urgency=medium

  * Fix installation of debian/proxmark3-doc.docs
  * New upstream version 4.14434+git20211007
  * Revert "Build several firmwares"
  * Remove patches merged upstream
  * Built correctly all the firmwares
  * Remove useless update-alternatives

 -- Sophie Brun <sophie@offensive-security.com>  Fri, 08 Oct 2021 10:55:52 +0200

proxmark3 (4.14434-0kali2) kali-dev; urgency=medium

  * Build several firmwares
  * Fix debian/control
  * Add update-alternatives for /usr/bin/proxmark3

 -- Sophie Brun <sophie@offensive-security.com>  Wed, 06 Oct 2021 11:30:30 +0200

proxmark3 (4.14434-0kali1) kali-dev; urgency=medium

  * Initial release (see: 5639)

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 04 Oct 2021 15:58:04 +0200
